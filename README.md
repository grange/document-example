# document-example

Example of the git workflow to use CI to manage a tex document, based on the ASTRON LaTeX class by John Swinbank: https://git.astron.nl/swinbank/astron-texmf

In principle the `Makefile` has several build methods that are also used in the CI/CD file. To get it to work correctly, one should set the DOCNAME variable to the name of the main `.tex` file. Also the `TEXMFHOME` variable should point to the location where the astron-texmf repo (subtirectory texmf) was cloned. 

## Makefile documentation
* `make` will compile the TeX file to PDF (output: `$(DOCNAME).pdf`)
* `make draft` will compile the TeX file, while adding the word *Draft* as a watermark (output: `$(DOCNAME).pdf`)
* `make diff` will compile a version of the document using `latexdiff-vc`, showing the differences between the head of the git repository and the current version on disk (output: `$(DOCNAME)-diff.pdf`). 
* `make diff COMMIT-COMMIT=5b51c` will compile a version of the document using `latexdiff-vc`, showing the differences between the specified commit and the current version on disk (output: `$(DOCNAME)-diff.pdf`). 
* `make clean` will remove all LaTeX generated files, but will keep the `.pdf`s
* `make deepclean` will remove all LaTeX generated files, as well as the `.pdf`s

## CI/CD documentation
The CI/CD pipeline covers a few cases.
* For every push to the repository, `make` will be executed as a basic check of the commit not breaking compilation.
* When someone issues a merge request, the CI/CD will generate a draft with `make draft` and a diff between the last commit before the branch on which the request is based, and the last commit of the branch itself using `make diff` (with the COMMIT variable set appropriately). Those two files are published as artifacts which makes them available from the merge issue. 
* When a tag is created, `make` is executed. Then the release step can be executed manually. This step will make the PDF output file of `make` an asset in the release.

## Contributions
Feel free to propose changes, or to just fo a merge request to test the functionality (just mention that in the request description). 
