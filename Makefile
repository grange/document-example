DOCNAME=example
export TEXMFHOME ?= ./astron-texmf/texmf

$(DOCNAME).pdf: $(DOCNAME).tex meta.tex
	xelatex $(DOCNAME)
	makeglossaries $(DOCNAME)
	biber $(DOCNAME)
	xelatex $(DOCNAME)
	xelatex $(DOCNAME)

clean: 
	rm -f $(DOCNAME).aux $(DOCNAME).bbl $(DOCNAME).bcf $(DOCNAME).blg $(DOCNAME).glg $(DOCNAME).glo $(DOCNAME).gls $(DOCNAME).ist
	rm -f $(DOCNAME).log $(DOCNAME).out $(DOCNAME).run.xml $(DOCNAME).aux meta.tex
	rm -f $(DOCNAME)-diff.aux $(DOCNAME)-diff.bbl $(DOCNAME)-diff.bcf $(DOCNAME)-diff.blg $(DOCNAME)-diff.glg $(DOCNAME)-diff.glo $(DOCNAME)-diff.gls $(DOCNAME)-diff.ist
	rm -f $(DOCNAME)-diff.log $(DOCNAME)-diff.out $(DOCNAME)-diff.run.xml $(DOCNAME)-diff.aux $(DOCNAME)-diff.tex meta.tex texput.log

deepclean: clean
	rm -f $(DOCNAME).pdf $(DOCNAME)-diff.pdf

draft: $(DOCNAME).tex meta.tex
	xelatex "\let\ifdraft\iftrue\input{$(DOCNAME)}"
	makeglossaries $(DOCNAME)
	biber $(DOCNAME)
	xelatex "\let\ifdraft\iftrue\input{$(DOCNAME)}"
	xelatex "\let\ifdraft\iftrue\input{$(DOCNAME)}"

$(DOCNAME)-diff.tex: meta.tex
	latexdiff-vc --git -r $(COMMIT) $(DOCNAME).tex
	mv $(DOCNAME)-diff$(COMMIT).tex $(DOCNAME)-diff.tex
	
diff: $(DOCNAME)-diff.tex 
	xelatex $(DOCNAME)-diff
	makeglossaries $(DOCNAME)-diff
	biber $(DOCNAME)-diff
	xelatex $(DOCNAME)-diff
	xelatex $(DOCNAME)-diff

include ./astron-texmf/make/vcs-meta.make
